JDBC
****

Prog-01
-------


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Demo1 {

	public static void main(String[] args) {
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/fsd60";
		
		try {
		
			//1. Loading the MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			
			//2. Establishing the Connection
			con = DriverManager.getConnection(url, "root", "root");
			
			if (con != null) {
				System.out.println("Successfully Established the Connection");
				
				con.close();
				
			} else {
				System.out.println("Failed to Establish the Connection");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}



Run the program, you will be getting error like

Output
------
java.lang.ClassNotFoundException: com.mysql.cj.jdbc.Driver
	at java.net.URLClassLoader.findClass(Unknown Source)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	at sun.misc.Launcher$AppClassLoader.loadClass(Unknown Source)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	at java.lang.Class.forName0(Native Method)
	at java.lang.Class.forName(Unknown Source)
	at day01.Demo1.main(Demo1.java:16)




Download MySQL Connector with the version of your MySQL from the following Site:
https://downloads.mysql.com/archives/c-j/

Product Version : 8.0.33
Operating System: Platform Independent

Donwload the zip file not tar.gz (tar.gz is for linux) and (zip is for windows).


Extract MySQLConnector.8.0.33.zip and copy the mysql-connector-j-8.0.33.jar file into any of the location.



Add MySQL Connector to your JDBC Project under Eclipse
------------------------------------------------------
1. Right Click on the Project
2. Build Path -> Configure Build Path
3. Click on the Libraries Tab
4. Click on Add External Jar's Button
5. Navigate to the folder where you have copied the mysql-connector-j-8.0.33.jar file
6. Select mysql-connector-j-8.0.33.jar file and click on Apply and OK.
7. A saperate folder will be created under the JDBC Project under eclipse by the name "Referenced Libraries"
8. Expand "Referenced Libraries" folder, you can see the mysql-connector-j-8.0.33.jar file.


Now once again run the program, your program will be executed...






***************************************************************************





MySQL
-----
drop database fsd60;
create database fsd60;
use fsd60;

create table employee (
empId int(4) primary key,
empName varchar(20),
salary double(8, 2),
gender varchar(6),
emailId varchar(30) unique key,
password varchar(20));

insert into employee values 
(101, 'Harsha', 1212.12, 'Male',   'harsha@gmail.com', '123'),
(102, 'Pasha',  2323.23, 'Male',   'pasha@gmail.com',  '123'),
(103, 'Indira', 4545.45, 'Female', 'indira@gmail.com', '123'),
(104, 'Vamsi',  8989.89, 'Male',   'vamsi@gmail.com',  '123'),
(105, 'Venkat', 6565.65, 'Male',   'venkat@gmail.com', '123'),
(106, 'Gopi',   7878.78, 'Male',   'gopi@gmail.com',   '123');

select * from employee;






1. Create a package called com.db (common.database) under src package.
2. under com.db, create a class called DbConnection.
3. under DbConnection class write the following code:

DbConnection
------------
package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

	public static Connection getConnection() {
		
		Connection connection = null;
		String url = "jdbc:mysql://localhost:3306/fsd60";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(url, "root", "root");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		return connection;
	}
}





//Insert a record into the Employee Table


Prog-02
-------
package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo2 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		int empId = 107;
		String empName = "Utkarsh";
		double salary = 9898.98;
		String gender = "Male";
		String emailId = "utkarsh@gmail.com";
		String password = "123";
		
		String insertQuery = "insert into employee values (" + 
				empId + ", '" + empName + "', " + salary + ", '" + 
				gender + "', '" + emailId + "', '" + password + "')";
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}








//Update Utkarsh a record by salary = 25000 into the Employee Table
Prog-03
-------
package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

///Update Utkarsh Record and update salary to 25000
public class Demo3 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId and New Salary");
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		
		String updateQuery = "update employee set salary = " + salary + " where empId = " + empId;
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(updateQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Updated...");
			} else {
				System.out.println("Record Updation Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}














//Delete Utkarsh a record from Employee Table
Prog-04
-------
package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

///Delete Utkarsh Record from Employee Table
public class Demo4 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter EmployeeId: ");
		int empId = scanner.nextInt();
		
		String deleteQuery = "delete from employee where empId = " + empId;
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(deleteQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Deleted...");
			} else {
				System.out.println("Record Deletion Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}






Select All Records from Employee Table

Prog5
-----
package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

//Get All Records from Employee Table
public class Demo5 {
	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;

		String selectQuery = "select * from employee";

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);

			if (resultSet != null) {

				while(resultSet.next()) {

//					System.out.println("EmpId   : " + resultSet.getInt(1));
//					System.out.println("EmpName : " + resultSet.getString(2));
//					System.out.println("Salary  : " + resultSet.getDouble("salary"));
//					System.out.println("Gender  : " + resultSet.getString(4));
//					System.out.println("EmailId : " + resultSet.getString(5));
//					System.out.println("Password: " + resultSet.getString(6));
//					System.out.println();
					
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}









































