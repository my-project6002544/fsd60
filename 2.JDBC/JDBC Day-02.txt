JDBC
****

Day-02
------

GetEmployeeById
---------------
package day02;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo01 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		System.out.print("Enter Employee ID: ");
		int empId = new Scanner(System.in).nextInt();
		
		String selectQuery = "select * from employee where empId = " + empId;
		
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			
			if (resultSet.next()) {
				System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
				System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
				System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
			} else {
				System.out.println("Unable to Fetch the Employe Record!!!");
			}
			
			resultSet.close();
			statement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				if (connection != null) {					
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}





--------------------------------------------------------------------------------------------



Employee Login
--------------

package day02;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;


public class Demo02 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Email-Id: ");
		String emailId = scanner.next();
		System.out.print("Enter Password: ");
		String password = scanner.next();
		
		String loginQuery = "select * from employee where emailId = '" + emailId + "' and password = '" + password + "'";
		
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(loginQuery);
			
			if (resultSet.next()) {
				System.out.println("\nLoginStatus: Login Success...\n");
				
				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble(3));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
				
			} else {
				System.out.println("\nLoginStatus: Invalid Credentials!!!");
			}
			
			resultSet.close();
			statement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				if (connection != null) {					
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}





--Done with Statement Interface


*******************************************************************************



Prepared Statement
------------------



Insert an Employee Record
-------------------------

package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Insert an Employee Record
public class Demo03 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Employee Details");
		
		int empId = scanner.nextInt();
		String empName = scanner.next();
		double salary = scanner.nextDouble();
		String gender = scanner.next();
		String emailId = scanner.next();
		String password = scanner.next();
		System.out.println();
		
		String insertQuery = "Insert into employee values (?, ?, ?, ?, ?, ?)";
		
		try {
			preparedStatement = connection.prepareStatement(insertQuery);
						
			preparedStatement.setInt(1, empId);
			preparedStatement.setString(2, empName);
			preparedStatement.setDouble(3, salary);
			preparedStatement.setString(4, gender);
			preparedStatement.setString(5, emailId);
			preparedStatement.setString(6, password);
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Inserted into the table");
			} else {
				System.out.println("Record Insertion Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}





*******************************************************************************






Update Employee Salary
----------------------
package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Update an Employee Record
public class Demo04 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter EmployeeID and Salary");
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		System.out.println();
		
		String insertQuery = "Update employee set salary=? where empId=?";
		
		try {
			preparedStatement = connection.prepareStatement(insertQuery);
						
			preparedStatement.setDouble(1, salary);
			preparedStatement.setInt(2, empId);
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Updated...");
			} else {
				System.out.println("Record Updation Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}









*******************************************************************************















































































